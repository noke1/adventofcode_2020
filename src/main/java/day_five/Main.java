package day_five;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

// Day 5, Part one - https://adventofcode.com/2020/day/5
// Day 5, Part two - https://adventofcode.com/2020/day/5#part2


public class Main {

    public static void main(String[] args) {
        List<String> seatSequences = readInput("src/main/resources/five.txt");
        List<Seat> seats = resolveSeatPositions(seatSequences);
        int maxSeatId = getMaxSeatId(seats);
        System.out.printf("Part one answer: %d.%n", maxSeatId);

        int mySeatId = getMySeatId(seats);
        System.out.printf("Part two answer: %d.%n", mySeatId);
    }

    private static int getMySeatId(List<Seat> seats) {
        List<Integer> collect = seats.stream()
                .mapToInt(Seat::countSetId)
                .sorted()
                .boxed()
                .collect(Collectors.toList());
        
        int answer = 0;
        for (int i = 0; i < collect.size()-1; i++) {
            if(collect.get(i+1) - collect.get(i) != 1) return collect.get(i+1) - 1;
        }
        return answer;
    }

    private static int getMaxSeatId(List<Seat> seats) {
        return seats.stream()
                .mapToInt(Seat::countSetId)
                .max()
                .orElseThrow(NoSuchElementException::new);
    }

    private static List<Seat> resolveSeatPositions(List<String> seatSequences) {
        List<Seat> seats = new ArrayList<>();

        for (String seatSequence : seatSequences) {
            String rowSequence = seatSequence.substring(0, 7);
            String colSequence = seatSequence.substring(7);
            int seatRowPosition = countPosition(rowSequence,127,0);
            int seatColPosition = countPosition(colSequence, 7, 0);
            seats.add(new Seat(seatRowPosition, seatColPosition));
        }
        return seats;
    }

    private static int countPosition(final String sequence, int maxValue, int minValue) {
        Set<Character> upperHalf = new HashSet<>(Arrays.asList('R', 'B'));
        Set<Character> lowerHalf = new HashSet<>(Arrays.asList('L', 'F'));

        for (int i = 0; i < sequence.length(); i++) {
            char letter = sequence.charAt(i);
            if (upperHalf.contains(letter)) minValue = (int) (minValue + (Math.ceil(((double) maxValue - minValue) / 2)));
            if (lowerHalf.contains(letter)) maxValue = (int) (maxValue - (Math.ceil(((double) maxValue - minValue) / 2)));
        }
        if (maxValue != minValue)
            throw new IllegalArgumentException(String.format("Max and min are different, but they should be. Max: %d; Min: %d", maxValue, minValue));
        return maxValue;
    }

    public static List<String> readInput(String inputLocation) {
        List<String> input = new ArrayList<>();
        Scanner scanner;
        try {
            scanner = new Scanner(new File(inputLocation));
            StringBuilder sb = new StringBuilder();
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                input.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return input;
    }
}
