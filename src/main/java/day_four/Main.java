package day_four;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

// Day 4, Part one - https://adventofcode.com/2020/day/4
// Day 4, Part two - https://adventofcode.com/2020/day/4#part2


public class Main {

    public static void main(String[] args) {
        List<String> input = readInput("src/main/resources/four.txt");
        List<String> validPassportsData = removeInvalidPassportFromFile(input);
        System.out.printf("Part one: %d.%n", validPassportsData.size());

        List<Passport> passports = mapToPassports(validPassportsData);
        List<Passport> validPassports = validatePassports(passports);
        System.out.printf("Part two: %d.%n", validPassports.size());
    }

    private static List<Passport> validatePassports(List<Passport> passports) {
        return passports.stream()
                .filter(x -> x.getByr() >= 1920 && x.getByr() <= 2002)
                .filter(x -> x.getIyr() >= 2010 && x.getIyr() <= 2020)
                .filter(x -> x.getEyr() >= 2020 && x.getEyr() <= 2030)
                .filter(x -> isHeightCorrect(x.getHgt()))
                .filter(x -> isHairColorCorrect(x.getHcl()))
                .filter(x -> isEyeColorCorrect(x.getEcl()))
                .filter(x -> isPassportIdCorrect(x.getPid()))
                .collect(Collectors.toList());
    }

    private static boolean isPassportIdCorrect(String passportId) {
        return passportId.matches("[0-9]{9}");
    }

    private static boolean isEyeColorCorrect(String eyeColor) {
        Set<String> allowedColors = new HashSet<>(Arrays.asList("amb", "blu", "brn", "gry", "grn", "hzl", "oth"));
        return allowedColors.contains(eyeColor);
    }

    private static boolean isHairColorCorrect(String hairColor) {
        return hairColor.matches("^[#][0-9a-f]{6}");
    }

    private static boolean isHeightCorrect(String height) {
        int minHeightValueInCM = 150;
        int maxHeightValueInCM = 193;
        int minHeightValueInIN = 59;
        int maxHeightValueInIN = 76;

        if (height.contains("cm")) {
            String heightUnitRemoved = height.replace("cm", "");
            int heightValue = Integer.parseInt(heightUnitRemoved);
            if (heightValue >= minHeightValueInCM && heightValue <= maxHeightValueInCM) return true;
        }
        if (height.contains("in")) {
            String heightUnitRemoved = height.replace("in", "");
            int heightValue = Integer.parseInt(heightUnitRemoved);
            if (heightValue >= minHeightValueInIN && heightValue <= maxHeightValueInIN) return true;
        }
        return false;
    }

    private static List<String> removeInvalidPassportFromFile (List < String > input) {
        Predicate<String> containsAllFields = x -> x.contains("byr:") &&
                x.contains("iyr:") &&
                x.contains("eyr:") &&
                x.contains("hgt:") &&
                x.contains("hcl:") &&
                x.contains("ecl:") &&
                x.contains("pid:") &&
                x.contains("cid:");

        Predicate<String> containsFieldsExceptCid = x -> x.contains("byr:") &&
                x.contains("iyr:") &&
                x.contains("eyr:") &&
                x.contains("hgt:") &&
                x.contains("hcl:") &&
                x.contains("ecl:") &&
                x.contains("pid:");

        return input.stream()
                .filter(containsAllFields.or(containsFieldsExceptCid))
                .collect(Collectors.toList());
    }

    private static List<Passport> mapToPassports (List < String > validPassports) {
        List<Passport> passports = new ArrayList<>();
        for (String validPassport : validPassports) {
            String[] passportData = validPassport.split("\\s+");
            int byr = 0;
            int iyr = 0;
            int eyr = 0;
            String hgt = "";
            String hcl = "";
            String ecl = "";
            String pid = "";
            String cid = "";
            for (String field : passportData) {
                if (field.contains("byr:")) byr = Integer.parseInt(field.split(":")[1]);
                if (field.contains("iyr:")) iyr = Integer.parseInt(field.split(":")[1]);
                if (field.contains("eyr:")) eyr = Integer.parseInt(field.split(":")[1]);
                if (field.contains("hgt:")) hgt = field.split(":")[1];
                if (field.contains("hcl:")) hcl = field.split(":")[1];
                if (field.contains("ecl:")) ecl = field.split(":")[1];
                if (field.contains("pid:")) pid = field.split(":")[1];
                if (field.contains("cid:")) cid = field.split(":")[1];
            }
            passports.add(new Passport(byr, iyr, eyr, hgt, hcl, ecl, pid, cid));
        }
        return passports;
    }

    public static List<String> readInput (String inputLocation){
        Path fileName = Path.of(inputLocation);
        String actual = "";
        try {
            actual = Files.readString(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.stream(actual.split("\\n[\\n]+")).collect(Collectors.toList());
    }
}
