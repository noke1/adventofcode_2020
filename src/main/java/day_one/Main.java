package day_one;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

// Day 1, Part one - https://adventofcode.com/2020/day/1
// Day 1, Part two - https://adventofcode.com/2020/day/1#part2

public class Main {

    public static void main(String[] args) {
        int[] input = readInput("src/main/resources/one.txt");
        System.out.println("Part one answer: " + solveForTwoNumbers(input));
        System.out.println("Part two answer: " + solveForThreeNumbers(input));

    }

    private static int solveForThreeNumbers(int[] input) {
        int targetNumber = 2020;
        int result = 0;
        for (int i = 0; i < input.length; i++) {
            for (int j = i+1; j < input.length; j++) {
                for (int k = j+1; k < input.length; k++) {
                    if (input[i] + input[j] + input[k] == targetNumber) {
                        result = input[i] * input[j] * input[k];
                        break;
                    }
                }
            }
        }
        return result;
    }

    private static int solveForTwoNumbers(int[] input) {
        int targetNumber = 2020;
        int result = 0;

        for (int i = 0; i < input.length; i++) {
            for (int j = i+1; j < input.length; j++) {
                if (input[i] + input[j] == targetNumber) {
                    result = input[i] * input[j];
                    break;
                }
            }
        }
        return result;
    }

    public static int[] readInput(String inputLocation) {
        ArrayList<Integer> fileContent = new ArrayList<>();
        Scanner scanner;
        try {
            scanner = new Scanner(new File(inputLocation));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                fileContent.add(Integer.parseInt(line));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fileContent.stream().mapToInt(x -> x).toArray();
    }
}
