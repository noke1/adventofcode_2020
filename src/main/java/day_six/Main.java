package day_six;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

// Day 6, Part one - https://adventofcode.com/2020/day/6
// Day 6, Part two - https://adventofcode.com/2020/day/6#part2

public class Main {

    public static void main(String[] args) {
        List<String> answers = readInput("src/main/resources/six.txt");
        int yesAnswers = countUniqueChars(answers);
        System.out.printf("Part one: %d.%n", yesAnswers);

        int everyoneYesAnswers = countEveryoneYesAnswers(answers);
        System.out.printf("Part one: %d.%n", everyoneYesAnswers);
    }

    private static int countEveryoneYesAnswers(List<String> answers) {
        List<Integer> allAnswersCounts = new ArrayList<>();

        for (String answer : answers) {
            int numberOfUsersAnswered = answer.split("\\s+").length;
            String spaceRemoved = answer.replaceAll("\\s+", "");

            Map<Character, Long> eachLetterFrequency = countEachLetterFrequency(spaceRemoved);
            List<Long> correctAnswers = countYesAnswersToAllQuestions(eachLetterFrequency, numberOfUsersAnswered);
            
            allAnswersCounts.add(correctAnswers.size());
        }
        return allAnswersCounts.stream().mapToInt(Integer::intValue).sum();
    }

    private static List<Long> countYesAnswersToAllQuestions(Map<Character, Long> eachLetterFrequency, int numberOfUsersAnswered) {
        return eachLetterFrequency.values().stream()
                .filter(x -> x == numberOfUsersAnswered)
                .collect(Collectors.toList());
    }

    private static Map<Character, Long> countEachLetterFrequency(String spaceRemoved) {
        return spaceRemoved.chars()
                .mapToObj(x -> (char) x)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    private static int countUniqueChars(List<String> answers) {
        List<Integer> uniquesInEachWord = new ArrayList<>();

        for (String answer : answers) {
            String spaceRemoved = answer.replaceAll("\\s+", "");
            int uniqueCharsInWordCount = (int) spaceRemoved.chars().distinct().count();
            uniquesInEachWord.add(uniqueCharsInWordCount);
        }

        return uniquesInEachWord.stream().mapToInt(Integer::intValue).sum();
    }


    public static List<String> readInput (String inputLocation){
        Path fileName = Path.of(inputLocation);
        String actual = "";
        try {
            actual = Files.readString(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.stream(actual.split("\\n[\\n]+")).collect(Collectors.toList());
    }
}
