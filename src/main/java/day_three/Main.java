package day_three;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Day 3, Part one - https://adventofcode.com/2020/day/3
// Day 3, Part two - https://adventofcode.com/2020/day/3#part2


public class Main {

    public static void main(String[] args) {
        List<String> input = readInput("src/main/resources/three.txt");
        System.out.println("Part one: " + countTreesPartOne(input));
        System.out.println("Part two: " + countTreesPartTwo(input));
    }


    private static int countTreesPartOne(List<String> input) {
        return countTrees(input, 3, 1);
    }

    private static long countTreesPartTwo(List<String> input) {
        int res1 = countTrees(input, 1, 1);
        int res2 = countTrees(input, 3, 1);
        int res3 = countTrees(input, 5, 1);
        int res4 = countTrees(input, 7, 1);
        int res5 = countTrees(input, 1, 2);
        System.out.println(res1 + "::" + res2 + "::" +res3 + "::" + res4 + "::" + res5);
        return (long) res1 * res2 * res3 * res4 * res5;
    }

    private static int countTrees(List<String> input, int slopeRightOffset, int slopeDownOffset) {
        int treesCount = 0;
        int mapWidth = input.get(0).length();
        int mapLength = input.size();
        int slopeStartPosition = 1;

        for (int i = slopeStartPosition, j = slopeRightOffset; i < mapLength; i+=slopeDownOffset, j = (j + slopeRightOffset) % mapWidth) {
            if (input.get(i).charAt(j) == '#') treesCount++;
        }
        return treesCount;
    }

    public static List<String> readInput(String inputLocation) {
        ArrayList<String> fileContent = new ArrayList<>();
        Scanner scanner;

        try {
            scanner = new Scanner(new File(inputLocation));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                fileContent.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return fileContent;
    }
}
