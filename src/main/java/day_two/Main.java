package day_two;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Day 2, part one -  https://adventofcode.com/2020/day/2
// Day 2, part two -  https://adventofcode.com/2020/day/2#part2

public class Main {

    public static void main(String[] args) {
        String[] input = readInput("src/main/resources/two.txt");
        System.out.println("Part one answer: " + solvePartOne(input));
        System.out.println("Part two answer: " + solvePartTwo(input));

    }

    private static int solvePartTwo(String[] input) {
        String[] removed = removeUnnecessaryChars(input);
        List<Policy> policies = mapToPolicies(removed);
        return countCorrectPoliciesPartTwo(policies);
    }

    private static int solvePartOne(String[] input) {
        String[] removed = removeUnnecessaryChars(input);
        List<Policy> policies = mapToPolicies(removed);
        return countCorrectPoliciesPartOne(policies);
    }

    private static int countCorrectPoliciesPartOne(List<Policy> policies) {
        int count = 0;
        for (Policy policy : policies) {
            int letterCount = 0;
            String password = policy.getPassword();
            for (int i = 0; i < password.length(); i++) {
                if (password.charAt(i) == policy.getExpectedLetter()) letterCount++;
            }
            if (letterCount >= policy.getMinOccurances() && letterCount <= policy.getMaxOccurances()) count++;
            letterCount = 0;
        }
        return count;
    }

    private static int countCorrectPoliciesPartTwo(List<Policy> policies) {
        return (int) policies.stream()
                .filter(x ->
                        x.getPassword().charAt(x.getMinOccurances() - 1) == x.getExpectedLetter() 
                        ^
                        x.getPassword().charAt(x.getMaxOccurances() - 1) == x.getExpectedLetter())
                .count();
    }

    private static List<Policy> mapToPolicies(String[] removed) {
        ArrayList<Policy> policies = new ArrayList<>();

        for (String s : removed) {
            String[] split = s.split("\\s+");
            for (int i = 0; i < split.length-3; i+=4) {
                Policy policy = new Policy(
                        Integer.parseInt(split[i]),
                        Integer.parseInt(split[i + 1]),
                        split[i + 2].charAt(0),
                        split[3]
                );
                policies.add(policy);
            }
        }
        return policies;
    }

    private static String[] removeUnnecessaryChars(String[] input) {
        for (int i = 0; i < input.length; i++) {
            input[i] = input[i].replaceAll("[:\\-]", " ");
        }
        return input;
    }

    public static String[] readInput(String inputLocation) {
        ArrayList<String> fileContent = new ArrayList<>();
        Scanner scanner;
        try {
            scanner = new Scanner(new File(inputLocation));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                fileContent.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fileContent.toArray(String[]::new);
    }
}
