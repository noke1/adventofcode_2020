package day_two;

public class Policy {
    private int minOccurances;
    private int maxOccurances;
    private char expectedLetter;
    private String password;

    public Policy(int minOccurances, int maxOccurances, char expectedLetter, String password) {
        this.minOccurances = minOccurances;
        this.maxOccurances = maxOccurances;
        this.expectedLetter = expectedLetter;
        this.password = password;
    }

    public int getMinOccurances() {
        return minOccurances;
    }

    public void setMinOccurances(int minOccurances) {
        this.minOccurances = minOccurances;
    }

    public int getMaxOccurances() {
        return maxOccurances;
    }

    public void setMaxOccurances(int maxOccurances) {
        this.maxOccurances = maxOccurances;
    }

    public char getExpectedLetter() {
        return expectedLetter;
    }

    public void setExpectedLetter(char expectedLetter) {
        this.expectedLetter = expectedLetter;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Policy{" +
                "minOccurances=" + minOccurances +
                ", maxOccurances=" + maxOccurances +
                ", expectedLetter=" + expectedLetter +
                ", password='" + password + '\'' +
                '}';
    }
}
